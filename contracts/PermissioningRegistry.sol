pragma solidity >=0.4.21 <0.6.0;

import "./interfaces/ISecretStorePermissioning.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/introspection/ERC165.sol";


contract PermissioningRegistry is ISecretStorePermissioning, Ownable, ERC165 {

    struct PermissionEntry {
        address[] users;
        address admin;
        bool exposed;
    }


    struct Request {
     address userAccount;
     address organisation;
    }

    
    mapping(bytes32 => PermissionEntry) public permissions;

    Request[] allRequests;


    event NewAdmin(bytes32 indexed document);
    
    event Permission(string indexed _title,
                        bytes32 indexed document);

    modifier onlyAdmins(bytes32 document) {
        require(_isAdmin(document), "Caller has to be the current admin or owner.");
        _;
    }

    constructor() public {
        // ERC 165 support
        _registerInterface(this.checkPermissions.selector);
        _registerInterface(
            this.owner.selector
            ^ this.isOwner.selector
            ^ this.renounceOwnership.selector
            ^ this.transferOwnership.selector
        );
    }


    function getRequests(address userAccount, uint ind) external payable returns (address) {
        uint j = 0;
        for (uint i = 0; i < allRequests.length; ++i) {
            if (allRequests[i].userAccount == userAccount && j == ind) {
                return allRequests[i].organisation;
            }
            j ++;
        }
        //need to check
        //changed 2018-09-10 T 07:38:40.516 IST
        return 0x0000000000000000000000000000000000000000;
    }


      function addRequest(address userAccount) external payable returns (bool) {
        for (uint i = 0; i < allRequests.length; ++i) {
            if (allRequests[i].userAccount == userAccount && allRequests[i].organisation == msg.sender) {
                return false;
            }
        }
        allRequests.length ++;
        allRequests[allRequests.length - 1] = Request(userAccount, msg.sender);
        return  true;
    }




    function removeRequest(address userAccount, address organisation) internal returns (bool) {
        uint j = 0;
        for (uint i = 0; i < allRequests.length; ++i) {
            if (allRequests[i].userAccount == userAccount && allRequests[i].organisation == organisation) {
                        for ( j = i + 1;j < allRequests.length; ++ j) {
                            allRequests[j - 1] = allRequests[j];
                        }
                        allRequests.length --;
                    }
                }
                return true;
            }
 



      function setAdmin(bytes32 document, address newAdmin) external onlyAdmins(document) returns (bool) {
        require(newAdmin != address(0), "New admin address cannot be 0x0.");
        permissions[document].admin = newAdmin;
        emit NewAdmin(document);
        return true;
    }

    function setUsers(bytes32 document, address[] _users) external onlyAdmins(document) returns (bool) {
        permissions[document].users = _users;
        emit Permission("setUsers",document);
        return true;
    }

    function setExposed(bytes32 document, bool _exposed) external onlyAdmins(document) returns (bool) {
        permissions[document].exposed = _exposed;
        emit Permission("setExosed", document);
        return true;
    }

    function addUser(bytes32 document, address newUser) external onlyAdmins(document) returns (bool) {
        permissions[document].users.push(newUser);
        removeRequest(permissions[document].admin, newUser);
        emit Permission("addUser", document);
        return true;
    }


        function removeUser(bytes32 document, address user) external onlyAdmins(document) returns (bool) {
        for (uint i = 0; i < permissions[document].users.length; ++ i) {
            if (permissions[document].users[i] == user) {
                for (uint j = i + 1;j < permissions[document].users.length; ++ j) {
                    permissions[document].users[j - 1] = permissions[document].users[j];
                }
                permissions[document].users.length --;
            }
        }      
        emit Permission("removedUser", document);
        return true;
    }



    function removePermission(bytes32 document) external onlyAdmins(document) returns (bool) {
        delete permissions[document];
        emit Permission("removePermission",document);
        return true;
    }

    function permission(bytes32 document, address[] _users) external returns (bool) {
        require(
            permissions[document].admin == address(0) || _isAdmin(document),
            "You have to be admin or owner to change permissions."
        );
        permissions[document].admin = msg.sender;
        permissions[document].users = _users;
        emit Permission("addPermission",document);
        return true;
    }

     //   internal function to compare strings

    function stringsEqual(string memory _a, string memory _b) internal pure returns (bool) {
bytes memory a = bytes(_a);
bytes memory b = bytes(_b);
if (a.length != b.length)
return false;
// @todo unroll this loop
for (uint i = 0; i < a.length; i ++)
{
if (a[i] != b[i])
return false;
}
return true;
}

    function checkPermissions(address user, bytes32 document) public view returns (bool) {
        if (!_isInitialized(document)) {
            return false;
        }
        
        if (permissions[document].exposed) {
            return true;
        }

        if (_isAdmin(document)) {
            return true;
        }
        
        address[] storage users = permissions[document].users;
        for (uint i = 0; i < users.length; i++) {
            if (users[i] == user)
                return true;
        }
        return false;
    }

    function getAdmin(bytes32 document) public view returns (address) {
        return permissions[document].admin;
    }

    function getUsers(bytes32 document) public view returns (address[]) {
        return permissions[document].users;
    }

    function isExposed(bytes32 document) public view returns (bool) {
        return permissions[document].exposed;
    }

    function _isAdmin(bytes32 document) internal view returns (bool) {
        return (permissions[document].admin == msg.sender || isOwner());
    }

    function _isInitialized(bytes32 document) internal view returns (bool) {
        return (permissions[document].admin != address(0));
    }
}