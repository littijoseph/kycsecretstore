# Design Decisions

## Why we chose Secret Store over normal way of smart contracts for KYC?

- Secret store is the encrypted way of implementing smart contracts and permissioning on the public/private blockchains. It allows us to store on the blockchain a key built using ECDSA algorithm and that too can be fragmented/divided into pieces. Key's regeneration and retrievals are controlled by a SmartContract. The key generation and regeneration process runs under a Threshold System - like 2 out of 3 nodes keeping the secret key's pieces should be joined together to regenerate the actual key and this makes nodes itself unable to read the keys on it’s own and makes your documents or secrets totally safe.

- In simple terms we are using a key that is split into pieces and stored in secret nodes. This key is regenerated from the pieces and is used to encrypt data/smart contract. For derypting the data or to call the smart contract functions, we have to use the same key regenerating from the pieces. THe key retrieval is restricted using a smart contract which enables us to control who has got access to the key to decrypt the data.

## Why Private Ethereum based on the Parity Ethereum Client?

- Secret store is implemented only by the Parity Ethereum Client. There is no such feature available right now with Geth or any other Ethereum client implementations.

## Front end design level limitations?

- Basic Angular front end without much stress given in the data validations. 

- User credentials and KYC document encrypted data currently stored in the LocalStorage as part of the easy implementation which on a production environment needs to be replaced with wallets and other user authentication/authorisation mechanisms.  


## Why use Node accounts instead of handling accounts in a wallet?

- Web3 1.0 provides more support to the personal accounts stored within the node itself. Also as part of the easiness to follow the secret store procedure for encrypting and decrypting the document, we choose node accounts. This can be on a enhanced version of the application replaced with wallet implementation without much impact on the existing logic. 

## Other challenges and decisions 

- The initial development was done using nodejs code which was to test out how to implement and use secret store. 

- Used `secretstore` - Parity Secret Store JS client (JS based API on top of the official secretstore RPC API)  (npm secretstore) during the initial testing phase but had to move out of it while trying to reuse it in the Angular application. This was due to the fs, net dependencies in the `secretstore` module which is not allowed for a browser based application. 
    - Had to rewrite the JS API in Typescript for using with Angular App. 


- Had to upgrade from the default version of web3 available with the Truffle framework (V 0.20) to the Web3 version V1.0.36-beta to use different new APIs defined like web3.eth.personal \- to access the node accounts. 

- Had to reduce the Secret Store Threshold to 1  secret node because of some technical communication issues while using 3 secret nodes which makes frequent drop down of the communication and thereby resulted in nodes unable to reach. 

