#!/bin/bash
RUST_LOG=secretstore=debug,secretstore_net=debug ../parity --config ./conf_dev_regulator1.toml db kill &
RUST_LOG=secretstore=trace,secretstore_net=trace ../parity --config ./conf_dev_regulator2.toml db kill &
RUST_LOG=secretstore=trace,secretstore_net=trace ../parity --config ./conf_dev_regulator3.toml db kill & 
RUST_LOG=secretstore=trace,secretstore_net=trace ../parity --config ./users.toml db kill 
# only works if script is executed from this file's location
FILEDIR=`dirname "$0"`

rm -rf $FILEDIR/db.dev_ss_regulator1/secretstore/db
rm -rf $FILEDIR/db.dev_ss_regulator2/secretstore/db
rm -rf $FILEDIR/db.dev_ss_regulator3/secretstore/db
rm -rf $FILEDIR/db.users/secretstore/db