# KYC Using Secret Store

## Description
Know your customer (KYC) checks are currently an extremely time consuming and costly affair. Banks and other Finance entities have to spend millions of dollars every year to keep up with KYC regulations or risk being fined heavily. Through KYC chain we aim to simplify this process to a great extent.

KYC-chain eliminates the redundant KYC checks that banks currently perform by maintaining a common secure database in a block-chain. The nature of a block-chain ensures that unauthorized changes to the data are automatically invalidated.

Currently we have built this registry over a Ethereum private network using parity client. Also the KYC information being stored in the encrypted form in the Ethereum blockchain, the actual KYC details cannot be viewed unless you have the access rights allowed by the owner of the KYC information.
  
## Dependencies

- [Parity client](https://github.com/paritytech/parity-ethereum), compiled with secretstore feature enabled
- [Truffle](https://github.com/trufflesuite/truffle): Only used for compiling permissioning contracts
- node, npm
- npm packages: web3.js@1.0.36-beta, yargs, crypto-js
 
## Setup

### Install truffle

```bash
npm install -g truffle
```

### Clone the repo

```bash
git clone https://gitlab.com/littijoseph/kycsecretstore
```

### Install node packages
```bash
cd KYCSecretStore
npm install
```

### Parity client

Following the [official tutorial](https://wiki.parity.io/Secret-Store-Tutorial-overview), you need to compile from source. Please use the latest master branch, as features/fixes are continuously being added(Optional as Build is included in the project):

```bash
git clone https://github.com/paritytech/parity
cd parity
cargo build --features secretstore --release
```

Then copy the `parity` binary from `<parity repo>/target/release` to the project root folder which is `KYCSecretStore` by default. The scripts are going to look for the client here.

## Where things are

- Configuration files for the Secret store nodes, their chain & SecreStore DBs : `nodes_ss_dev/` folder. Set up and run you SS nodes and normal node with a command.
- Contracts: `contracts/`. Contains `PermissionRegistry` permissioning contract that is used for managing access to keys in SS.
- Compiled contracts: `build/contracts/`
- Angular FrontEnd `KYCSecretStoreFrontEnd`


## How to use

### Instructions
 1. Start up secret store cluster with `./start.sh` in their respective folder(`nodes_ss_dev/`).
 2. Fund the account with ether using the command `./fund.sh 0x8a052c81929c1eea590796348489026c4e3fcec4` - (unlocked account in the normal node used to deploy contract) from `nodes_ss_dev` folder.
 3. Compile contracts with `truffle compile`(Optional).
 4. Migrate the contracts using `truffle migrate --network dev --reset`.
 5. Run the angular project from `KYCSecretStoreFrontEnd` folder using `ng serve` command.

####  Handling Secret Store Nodes

You can easily start this cluster of nodes by running `./start.sh` in the nodes_ss_dev folder.

In the `nodes_ss_dev` folder you can:

 - Edit configuration files of 3 different SS nodes (Regulator1, Regulator2, Regulator3) which are fully connected: `conf_dev_[regulator1, regulator2, regulator3].toml`. They use separate db folders, and write logs in `<db folder name>/parity.log`. Accounts with password files are there too which are currently unlocked for the development purpose.

 - Start nodes
   ```bash
   ./start.sh
   ```
 - Stop nodes
   ```bash
   ./stop.sh
   ```
 - Clean SS db / chain db
   ```bash
   ./clear.sh
   ```
   This might be needed if we need to rebuild the network fresh.

 - Send test tokens from the rich account in dev networks. E.g.:
   ```bash
   ./fund.sh 0x8a052c81929c1eea590796348489026c4e3fcec4
   ```
   10 Ethers is sent to the account. Modify the rich account in the script if needed. Also provide the necessary account to supply ether to that account.