module.exports = {
  networks: {
    dev: {
      host: "127.0.0.1",
      port: 8550,
      network_id: "*", // Match any network id
      from: "0x8a052c81929c1eea590796348489026c4e3fcec4",
      gas: 8000000,
      gasPrice: 100000000000
    }
  },
  compilers: {
    solc: {
      version: "0.4.24",
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  }
};