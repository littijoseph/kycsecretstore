#!/usr/bin/env node

"use strict";
const utils = require("../src/utils.js");

const path = require("path");
const Web3 = require("web3");

const contractFirstStem = "Permissioning";
const contractFirstStemLower = contractFirstStem.toLowerCase();
const contractDynamic = "PermissioningDynamic";
const contractFireAndForget = "PermissioningFireAndForget";
const contractNoDoc = "PermissioningNoDoc";
const contractStatic = "PermissioningStatic";

const contractAddress = '0x39e952542c16F7EaF5500e786AbcF245BE49E97f';
const deployAccount = "0x625126096e7e89189bfe1aee8e442dcb3ee03e58";

async function deployPermission(args) {
    return utils.__awaiter(this, void 0, void 0, function* () {
        console.log("insid deloy permission");

        let argselector = {};
        argselector[contractDynamic] = [];
        argselector[contractFireAndForget] = [args.docid, args.accounts];
        argselector[contractNoDoc] = [args.accounts];
        argselector[contractStatic] = [];

        const web3 = new Web3(args.rpc);

        const {
            encryptionPwd
        } = yield utils.passwords();


        let contractName;

        // case sensitive..
        if (args.contract.startsWith(contractFirstStem)) {
            contractName = args.contract;
        } else {
            contractName = contractFirstStem + args.contract.charAt(0) + args.contract.substr(1);
        }


                let acc;
                if (args.acc === undefined) {
                    acc = deployAccount;
                } else {
                    acc = args.a;
                }

        const ContractJSON = require(path.join(__dirname, "../build/contracts/" + contractName + ".json"));

        console.log(contractName);


        let Contract = new web3.eth.Contract(ContractJSON.abi, contractAddress, {
            data: ContractJSON.bytecode
        });

        let from = args.from;
        // if (undefined === from) {
        //     let localAccounts = await web3.eth.getAccounts();
        //     if (localAccounts !== undefined && localAccounts.length !== 0) {
        //         web3.eth.defaultAccount = localAccounts[0];
        //         from = web3.eth.defaultAccount;
        //     }
        // }
        // let contract = await Contract.deploy({arguments: argselector[ContractJSON.contractName]})
        //     .send({
        //         from: from,
        //         gas: 8000000
        //     });




        // Contract = new web3.eth.Contract(SSPermissionsComplex.abi, '0x39e952542c16F7EaF5500e786AbcF245BE49E97f', {
        //     data: SSPermissionsComplex.bytecode
        // });
        // contractPermissions = yield Contract.deploy({arguments: [args.docid, accs]}).send({
        //     from: deployer,
        // });

        let contractPermissions = yield Contract.deploy({
            arguments: [args.docid, acc]
        }).encodeABI();

        console.log(contractPermissions);


        let transactionObject = {
            data: contractPermissions,
            from: deployAccount
        };

        web3.eth.personal.sendTransaction(transactionObject, encryptionPwd).then(console.log);







        //console.log(contractName + " deployed at: " + contract.options.address);
        return;
    });
}

module.exports = deployPermission;