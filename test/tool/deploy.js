const utils = require("../utils.js");
const path = require("path");

const httpRPC = "http://localhost:8550";

// const SSPermissions = require(path.join(__dirname, "../../build/contracts/SSPermissions.json"));
// const SSPermissionsSimple = require(path.join(__dirname, "../../build/contracts/SSPermissionsSimple.json"));
// const SSPermissionsSimpleNodoc = require(path.join(__dirname, "../../build/contracts/SSPermissionsSimpleNodoc.json"));
// const SSPermissionsComplex = require(path.join(__dirname, "../../build/contracts/SSPermissionsComplex.json"));

const PermissioningRegistry = require(path.join(__dirname, "../../build/contracts/PermissioningRegistry.json"));

// Parity local dev network's prefunded rich account
const richAccount = "0x00a329c0648769A73afAc7F9381E08FB43dBEA72";

const deployAccount = "0x625126096e7e89189bfe1aee8e442dcb3ee03e58";

// Hash of "mySecretDocument" as in the tutorial
const defaultDocID = "0x45ce99addb0f8385bd24f30da619ddcc0cadadab73e2a4ffb7801083086b3fc2";

const args = require("yargs")
    .usage('Usage: $0 [options]')
    .option('mode', {
        type: 'number',
        desc: "which Smart Contract to deploy. 0: default = provided in the official tutorial with hard coded values, 1: simple = the doc ID and accounts can be chosen, 2: complex = registry - an arbitrary number of ID's and accounts can be added, 3: simple nodoc = same as 'simple' but docID's don't matter",
        choices: [0, 1, 2, 3],
        demandOption: false,
        alias: "m",
        default: 0
    })
    .option('docid', {
        type: 'string',
        desc: "The document's ID to permission",
        demandOption: false,
        alias: "d",
        default: defaultDocID
    })
    .option('accounts', {
        type: 'array',
        desc: "Accounts that have access to this document's keys. Defaults to the accounts of Alice and Bob used in the tutorial.",
        demandOption: false,
        alias: "a",
        default: []
    })
    .option('from', {
        type: 'string',
        desc: "Deployer account. Defaults to pre-fundend account on the test network, and default account on Tobalaba.",
        demandOption: false,
        alias: "f",
        default: undefined
    })
    .argv;

function deployPermissioningContract() {
    return utils.__awaiter(this, void 0, void 0, function* () {
        const Web3 = require("web3");
        const web3 = new Web3(httpRPC);


        const {
            encryptionPwd,
            alicepwd,
            bobpwd,
            charliepwd
        } = yield utils.passwords();

        let accs;
        if (args.a === undefined || args.a.length === 0) {
            accs = deployAccount;
        } else {
            accs = args.a;
        }


        let deployer = args.from;
        console.log(args);
        const nId = yield web3.eth.net.getId();

        if (deployer === undefined) {

            // we need to unlock the deployer account
            web3.eth.defaultAccount = richAccount;
            deployer = deployAccount;
            web3.eth.personal.unlockAccount(deployer, "");

        }


        // deploy the permisioning contract
        if (args.mode === 1) {
            console.log("Deploying simple:")
            Contract = new web3.eth.Contract(SSPermissionsSimple.abi, {
                data: SSPermissionsSimple.bytecode
            });
            contractPermissions = yield Contract.deploy({
                arguments: [args.docid, accs]
            }).send({
                from: deployer,
            });
        } else if (args.mode === 2) {

            console.log("Deploying complex:");


            Contract = new web3.eth.Contract(PermissioningRegistry.abi, '0x39e952542c16F7EaF5500e786AbcF245BE49E97f', {
                data: PermissioningRegistry.bytecode
            });

            Contract.events.Permission({}, (error, event) => {

                    console.log("basic event :" + event);
                })
                // .on('confirmation', (event) => {
                //     console.log("event with confirmation :" + event); // same results as the optional callback above
                // })
                .on('data', (event) => {
                    console.log("event with data :" + event); // same results as the optional callback above
                })
                .on('changed', (event) => {
                    // remove event from local database
                })
                .on('error', console.error);

            let contractPermissions = yield Contract.deploy({
                arguments: [args.docid, accs]
            }).send({
                from: deployer,
                gas: 3000000
            }).on('transactionHash', (hash) => {
                console.log('hash: ' + hash);
            });
            // .on('confirmation', (confirmationNumber, receipt) => {
            //     console.log(res.json(receipt.events));
            // }).on('error', console.error);

            // myContract.once('Permission', {}, (error, event) => {
            //     console.log("once event" + event);
            // });

            console.log("contractPermissions.options.address");
            // contractPermissions = yield Contract.deploy({
            //     arguments: [args.docid, accs]
            // }).encodeABI();

            // console.log(contractPermissions);


            // let transactionObject = {
            //     data: contractPermissions,
            //     from: deployer
            // };

            // web3.eth.personal.sendTransaction(transactionObject, encryptionPwd).then(console.log);

        } else if (args.mode === 3) {
            console.log("Deploying simple nodoc:")
            Contract = new web3.eth.Contract(SSPermissionsSimpleNodoc.abi, {
                data: SSPermissionsSimpleNodoc.bytecode
            });
            contractPermissions = yield Contract.deploy({
                arguments: [accs]
            }).send({
                from: deployer,
            });
        } else {
            console.log("Deploying default:")
            Contract = new web3.eth.Contract(SSPermissions.abi, {
                data: SSPermissions.bytecode
            });
            contractPermissions = yield Contract.deploy().send({
                from: deployer,
            });
        }
        // console.log(contractPermissions.options.address);
    });
}

deployPermissioningContract();