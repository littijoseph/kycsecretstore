const fs = require("fs");
const secretstore = require("secretstore");

const utils = require("../utils.js");

const {
    httpRpcCommon
} = utils.connectionsHTTPRPC();

const {
    httpSSRegulator1,
    httpSSRegulator2,
    httpSSRegulator3
} = utils.connectionsHTTPSS();

function decryptKYC(accountaddress, accountpwd) {
    return utils.__awaiter(this, void 0, void 0, function* () {
        const Web3 = require("web3");
        const web3 = new Web3(httpRpcCommon);
        const ss = new secretstore.SecretStore(web3, httpSSRegulator1);

        console.log("account address: " + accountaddress);
        console.log("account password: " + accountpwd);

        // //create a new account 
        // recoveryPhrase = "stylus outing overhand dime radial seducing harmless uselessly evasive tastiness eradicate imperfect";
        // password = "test1";

        // const createdAccountAddress = yield ss.newAccountFromPhrase(recoveryPhrase,password);
        // console.log("CreatedAccount:"+createdAccountAddress);
        

        // Bob receives the message: document ID and encrypted document
        const messageReceived = JSON.parse(fs.readFileSync("./sent_message.json"));
        console.log("Message received: " + JSON.stringify(messageReceived));

        // 1. signing the document ID by Bob
        const signedDoc = yield ss.signRawHash(accountaddress, accountpwd, messageReceived.docID);
        console.log("Doc ID signed: " + signedDoc);

        // 2. Let's retrieve the keys
        const decryptionKeys = yield ss.session.shadowRetrieveDocumentKey(messageReceived.docID, signedDoc);
        console.log("Decryption keys retrieved: " + JSON.stringify(decryptionKeys));

        // 3. Decrypt document
        //decryptedSecret, commonPoint, decryptShadows, encryptedDocument
        const hexDocument = yield ss.shadowDecrypt(accountaddress, accountpwd,
            decryptionKeys.decrypted_secret,
            decryptionKeys.common_point,
            decryptionKeys.decrypt_shadows,
            messageReceived.encryptedDocument);
        console.log("Decrypted hex document: " + hexDocument);

        // 3.1 hex to str
        const document = web3.utils.hexToUtf8(hexDocument);
        console.log("Decrypted document: " + document);

    });
}

function getAccountDetailsandDecrypt() {
    return utils.__awaiter(this, void 0, void 0, function* () {
        const {
            encryptionAccount,
            decryptionAccount
        } = yield utils.accounts();

        const {
            encryptionPwd,
            decryptionPwd
        } = yield utils.passwords();

        decryptKYC(encryptionAccount, encryptionPwd);


    });
}

getAccountDetailsandDecrypt();