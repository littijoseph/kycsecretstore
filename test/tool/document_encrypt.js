const utils = require("../utils.js");
const fs = require("fs");

const secretstore = require("secretstore");

const {
    httpRpcCommon
} = utils.connectionsHTTPRPC();

const {
    httpSSRegulator1,
    httpSSRegulator2,
    httpSSRegulator3
} = utils.connectionsHTTPSS();

/**
 * Function to encrypt document 
 * 
 * @param {string} document document data
 * @param {string} accountaddress user account to use for encryption
 * @param {string} accountpwd user account password to use for encryption
 */
function encryptKYC(document, accountaddress, accountpwd) {

    return utils.__awaiter(this, void 0, void 0, function* () {

        const Web3 = require("web3");
        const web3 = new Web3(httpRpcCommon);
        const ss = new secretstore.SecretStore(web3, httpSSRegulator1);

        console.log("document: " + document);
        console.log("account address: " + accountaddress);
        console.log("account password: " + accountpwd);


        let messageToSend = {}

        // 1. we generate a hash of the document name as the document ID
        const docID = yield utils.getSHA256hash(document);
        console.log("doc ID: " + docID);

        messageToSend.docID = docID;

        // 2.1 we sign the document key id
        const signedDocID = yield ss.signRawHash(accountaddress, accountpwd, docID);
        console.log("signed doc ID: " + signedDocID);

        // 2.2 we generate the secret store server key
        let serverKey
        try {
            // threshold is chosen to be 1 like in the official tutorial
            serverKey = yield ss.session.generateServerKey(docID, signedDocID, 1);
        } catch (error) {
            if (error instanceof secretstore.SecretStoreSessionError &&
                error.response.body === '"\\"Server key with this ID is already generated\\""') {
                console.log("Erase Secure Store database or use different one.")
                throw error;
            }
            throw error;
        }
        console.log("Server key public part: " + JSON.stringify(serverKey));

        // 3. Generate document key
        const documentKey = yield ss.generateDocumentKey(accountaddress, accountpwd, serverKey);
        console.log("Document key" + JSON.stringify(documentKey));

        // 4.-1 the document in hex format
        const hexDocument = web3.utils.toHex(document);
        console.log("Hex document: " + hexDocument);

        // 4. Document encryption
        const encryptedDocument = yield ss.encrypt(accountaddress, accountpwd, documentKey.encrypted_key, hexDocument);
        console.log("Encrypted secret document: " + encryptedDocument);

        messageToSend.encryptedDocument = encryptedDocument;

        // 5. Store the generated document key
        let res = yield ss.session.storeDocumentKey(docID, signedDocID, documentKey.common_point, documentKey.encrypted_point);

        fs.writeFileSync("./sent_message.json", JSON.stringify(messageToSend));
    });
}


function getAccountDetailsandEncrypt() {
    return utils.__awaiter(this, void 0, void 0, function* () {

        const {
            encryptionAccount,
            decryptionAccount
        } = yield utils.accounts();

        const {
            encryptionPwd,
            decryptionPwd
        } = yield utils.passwords();

        const document = "fdsf to ttt";

        encryptKYC(document, decryptionAccount, decryptionPwd);


    });
}

getAccountDetailsandEncrypt();