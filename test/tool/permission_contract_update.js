const utils = require("../utils.js");

const httpRPC = "http://localhost:8550";

// Hash of "mySecretDocument" as in the tutorial
// const defaultDocID = "0x45ce99addb0f8385bd24f30da619ddcc0cadadab73e2a4ffb7801083086b3fc2";
const defaultDocID = "0xd9b5f58f0b38198293971865a14074f59eba3e82595becbe86ae51f1d9f1f65e";

// const checkFunctionName = "checkPermissions(address,bytes32)";
// const addPermissionAbi = {
//     "constant": false,
//     "inputs": [{
//             "name": "docID",
//             "type": "bytes32"
//         },
//         {
//             "name": "users",
//             "type": "address[]"
//         }
//     ],
//     "name": "addPermission",
//     "outputs": [],
//     "payable": false,
//     "stateMutability": "nonpayable",
//     "type": "function"

// };



const args = require("yargs")
    .usage('Usage: $0 [options]')
    .option('address', {
        type: 'string',
        desc: "The address of the permission contract.",
        demandOption: false,
        alias: "a"
    })
    .option('docid', {
        type: 'string',
        desc: "The document's ID to set permissions for",
        demandOption: false,
        alias: "d",
        default: defaultDocID
    })
    .option('accounts', {
        type: 'array',
        desc: "Accounts to set permission for the given document ID. Defaults to the accounts of Alice, Bob and Charlie used in the tutorial.",
        demandOption: false,
        alias: "ac",
        default: []
    })
    .argv;

function setPermissions() {


    return utils.__awaiter(this, void 0, void 0, function* () {
        const web3 = new(require("web3"))(httpRPC);

        //const { alice, bob, charlie } = yield utils.accounts(web3);
        const {
            encryptionAccount,
            decryptionAccount
        } = yield utils.accounts();

        console.log(" account:" + decryptionAccount);

        let accs;
        if (args.ac === undefined || args.ac.length === 0) {
            accs = [decryptionAccount];
        } else {
            accs = args.ac;
        }

    const {
        encryptionPwd,
        decryptionPwd
    } = yield utils.passwords();




        const {
            permissionABI
        } = yield utils.getABI();

        const {
            permissioningContractAddress
        } = utils.getPermissionContractAddress();


    
        console.log("permissioningContractAddress" + permissioningContractAddress);

        // // check the permisioning contract
        // const permissionContractInstance = new web3.eth.Contract(permissionABI, args.address[0]);

        // // using the promise
        // permissionContractInstance.methods.addPermission(utils.add0x(args.docid), accs).send({
        //         from: deployAccount
        //     })
        //     .then((receipt) => {
        //         console.log(receipt);
        //     });
        // check the permisioning contract


        const permissionContractInstance = new web3.eth.Contract(permissionABI, permissioningContractAddress);

        console.log("docid: " + args.docid);
        console.log("accs: " + accs);


        // using the promise
        // var contractPermissions = yield permissionContractInstance.methods.setAdmin(args.docid, accs).encodeABI();
        web3.eth.personal.unlockAccount(decryptionAccount, decryptionPwd);
        var contractPermissions = yield permissionContractInstance.methods.permission(args.docid, accs).send({
            from: decryptionAccount,
        });
        web3.eth.personal.lockAccount(decryptionAccount);
        // let transactionObject = {
        //     data: contractPermissions,
        //     from: encryptionAccount
        // };

        // permissionContractInstance.events.Permission({}, (error, event) => {

        //         console.log("basic event :" + event);
        //     })
        //     .on('data', (event) => {
        //         console.log("event with data :"+event); // same results as the optional callback above
        //     })
        //     .on('changed', (event) => {
        //         // remove event from local database
        //     })
        //     .on('error', console.error);

        // web3.eth.personal.sendTransaction(transactionObject, encryptionPwd).then(console.log);


        // permissionContractInstance.addPermission(utils.add0x(args.docid), accs, {
        //     //        contractInstance.addCustomer.(usnm, Data, {
        //     from: deployAccount,
        //     gas: 4700000
        // });

        //console.log(result);
        //console.log(acc + ": " + (web3.utils.hexToNumber(result) === 1));

    });
}

function setAdmin() {


    return utils.__awaiter(this, void 0, void 0, function* () {
        const web3 = new(require("web3"))(httpRPC);

        //const { alice, bob, charlie } = yield utils.accounts(web3);
        const {
            encryptionAccount,
            decryptionAccount
        } = yield utils.accounts();

        console.log(" account:" + encryptionAccount);

        let accs;
        if (args.ac === undefined || args.ac.length === 0) {
            accs = decryptionAccount;
        } else {
            accs = args.ac;
        }





        const {
            permissionABI
        } = yield utils.getABI();

        const {
            permissioningContractAddress
        } = utils.getPermissionContractAddress();


        const {
            encryptionPwd,
            decryptionPwd
        } = yield utils.passwords();

        console.log("permissioningContractAddress" + permissioningContractAddress);

        // // check the permisioning contract
        // const permissionContractInstance = new web3.eth.Contract(permissionABI, args.address[0]);

        // // using the promise
        // permissionContractInstance.methods.addPermission(utils.add0x(args.docid), accs).send({
        //         from: deployAccount
        //     })
        //     .then((receipt) => {
        //         console.log(receipt);
        //     });
        // check the permisioning contract


        const permissionContractInstance = new web3.eth.Contract(permissionABI, permissioningContractAddress);

        console.log("docid: " + args.docid);
        console.log("accs: " + accs);


        // using the promise
        // var contractPermissions = yield permissionContractInstance.methods.setAdmin(args.docid, accs).encodeABI();
        web3.eth.personal.unlockAccount(encryptionAccount, encryptionPwd);
        var contractPermissions = yield permissionContractInstance.methods.setAdmin(args.docid, accs).send({
            from: encryptionAccount,
        });
        web3.eth.personal.lockAccount(encryptionAccount);
        // let transactionObject = {
        //     data: contractPermissions,
        //     from: encryptionAccount
        // };

        // permissionContractInstance.events.Permission({}, (error, event) => {

        //         console.log("basic event :" + event);
        //     })
        //     .on('data', (event) => {
        //         console.log("event with data :"+event); // same results as the optional callback above
        //     })
        //     .on('changed', (event) => {
        //         // remove event from local database
        //     })
        //     .on('error', console.error);

        // web3.eth.personal.sendTransaction(transactionObject, encryptionPwd).then(console.log);


        // permissionContractInstance.addPermission(utils.add0x(args.docid), accs, {
        //     //        contractInstance.addCustomer.(usnm, Data, {
        //     from: deployAccount,
        //     gas: 4700000
        // });

        //console.log(result);
        //console.log(acc + ": " + (web3.utils.hexToNumber(result) === 1));

    });
}

 setPermissions();
// setAdmin();