import { TestBed } from '@angular/core/testing';

import { EthereumServicesService } from './ethereum-services.service';

describe('EthereumServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EthereumServicesService = TestBed.get(EthereumServicesService);
    expect(service).toBeTruthy();
  });
});
