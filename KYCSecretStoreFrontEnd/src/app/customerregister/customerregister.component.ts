import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';

@Component({
  selector: 'app-customerregister',
  templateUrl: `./customerregister.component.html`,
  styles: []
})
export class CustomerregisterComponent implements OnInit {
  public errorMsg: any;
  public customerFullName: string = '';
  public customerID: string = '';
  public customerPassword: string = '';
  constructor(private router: Router, private Service: EthereumServicesService) { }

  ngOnInit() {
  }

  async customerregister(event) {
    console.log(this.customerFullName);

    //   let documentToDecrypt = {
    //     "docID": "251c772a69eea7797733e453d0d620e92444d52c00851f414658cec86430fdcf",
    //      "encryptedDocument": "0xd74d913fcadf83b91babb4a3b496da7e6fbeea1192b2a0ae45"
    //     };


    //  documentToDecrypt = await this.Service.encryptDocument(this.customerFullName, "0x8a052c81929c1eea590796348489026c4e3fcec4", "common")

    //  await this.Service.decryptDocument(documentToDecrypt, "0x8a052c81929c1eea590796348489026c4e3fcec4", "common");

    const result = await this.Service.createAccountCustomer(this.customerFullName, this.customerID, this.customerPassword)
      .catch(error => {
        console.log(error);
      });
      if (result != null) {
    alert(this.customerFullName + " has been successfully added to the network!");
    this.router.navigate(['/customer/customerlogin']);
    }
  }

}

