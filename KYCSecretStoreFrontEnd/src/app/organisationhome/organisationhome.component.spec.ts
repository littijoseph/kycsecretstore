import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationhomeComponent } from './organisationhome.component';

describe('OrganisationhomeComponent', () => {
  let component: OrganisationhomeComponent;
  let fixture: ComponentFixture<OrganisationhomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationhomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
