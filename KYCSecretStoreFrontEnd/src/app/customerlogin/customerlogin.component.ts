import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';
@Component({
  selector: 'app-customerlogin',
  templateUrl: `./customerlogin.component.html`,
  styles: []
})
export class CustomerloginComponent implements OnInit {
  public errorMsg: any;
  public customerID: string = '';
  public customerPassword: string = '';

  constructor(private router: Router, private Service: EthereumServicesService) { }

  ngOnInit() {
  }
  customerlogin(event) {
    let savedCustomerDetails = this.Service.customerLogin(this.customerID, this.customerPassword);
    if (savedCustomerDetails != null) {
      alert(this.customerID + ' Logged in successfully');
      this.router.navigate(['/customerhome/addkycdata']);
    }
    else{
      alert("Incorrect credentials");
      this.customerID = '';
      this.customerPassword = '';
    }
  }
}
