import { Component, OnInit } from '@angular/core';
import { Organisation } from '../interfaces/organisation';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';

@Component({
  selector: 'app-organisationdetails',
  templateUrl: './organisationdetails.component.html',
  styles: []
})
export class OrganisationdetailsComponent implements OnInit {
  
  public currentOrganisation: Organisation;

  constructor(private router: Router, private Service: EthereumServicesService) {
    this.currentOrganisation = <Organisation>{};
   }

  ngOnInit() {
    this.currentOrganisation = this.Service.getOrganisationAccountDetails();
  }

}
