import { TestBed } from '@angular/core/testing';

import { FundingHelperService } from './funding-helper.service';

describe('FundingHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FundingHelperService = TestBed.get(FundingHelperService);
    expect(service).toBeTruthy();
  });
});
