import { TestBed } from '@angular/core/testing';

import { DocumentEncryptHelperService } from './document-encrypt-helper.service';

describe('DocumentEncryptHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentEncryptHelperService = TestBed.get(DocumentEncryptHelperService);
    expect(service).toBeTruthy();
  });
});
