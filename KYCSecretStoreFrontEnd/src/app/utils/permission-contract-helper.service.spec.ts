import { TestBed } from '@angular/core/testing';

import { PermissionContractHelperService } from './permission-contract-helper.service';

describe('PermissionContractHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PermissionContractHelperService = TestBed.get(PermissionContractHelperService);
    expect(service).toBeTruthy();
  });
});
