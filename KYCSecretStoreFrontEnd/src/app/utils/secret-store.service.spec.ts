import { TestBed } from '@angular/core/testing';

import { SecretStoreService } from './secret-store.service';

describe('SecretStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecretStoreService = TestBed.get(SecretStoreService);
    expect(service).toBeTruthy();
  });
});
