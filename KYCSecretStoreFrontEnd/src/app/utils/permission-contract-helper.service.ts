import { Injectable } from '@angular/core';
import { UtilsHelperService } from './utils-helper.service';

//import * as Web3 from 'web3';

import Web3 from 'web3';

import { AccountDetails } from '../interfaces/account-details';
import { Permission } from '../interfaces/permission';
import { Organisation } from '../interfaces/organisation';

@Injectable({
  providedIn: 'root'
})
export class PermissionContractHelperService {

  private permissionContractInstance: any;
  private web3: any;
  private permissionABI;

  constructor(private utils: UtilsHelperService) {
    this.web3 = new Web3(new Web3.providers.HttpProvider(this.utils.HTTP_RPC_COMMON));

  }


  async setAdminPermission(account: AccountDetails) {

    console.log("permissioningContractAddress" + this.utils.PERMISSION_CONTRACT);


    // check the permisioning contract
    this.permissionABI = await this.utils.getABI();
    console.log("ABI:" + JSON.stringify(this.permissionABI));

    const permissionContractInstance = new this.web3.eth.Contract(this.permissionABI, this.utils.PERMISSION_CONTRACT);
    // var permissionContractInstance = permissionContract.at(this.utils.PERMISSION_CONTRACT);


    console.log("docid: " + account.documentID);
    console.log("accs: " + account.accountID);


    // using the promise
    // var contractPermissions = yield permissionContractInstance.methods.setAdmin(args.docid, accs).encodeABI();
    this.web3.eth.personal.unlockAccount(account.accountAddress, account.accountPwd);
    let rec = await permissionContractInstance.methods.permission(this.utils.add0x(account.documentID), [account.accountAddress]).send(
      {
        from: account.accountAddress,
      });
    console.log(rec.status);

    this.web3.eth.personal.lockAccount(account.accountAddress);
  }


  async addPermission(newPermission: Permission, adminAccount: AccountDetails) {

    console.log("permissioningContractAddress" + this.utils.PERMISSION_CONTRACT);


    // check the permisioning contract
    this.permissionABI = await this.utils.getABI();
    const permissionContractInstance = new this.web3.eth.Contract(this.permissionABI, this.utils.PERMISSION_CONTRACT);

    console.log("docid: " + newPermission.documentID);
    console.log("accs: " + newPermission.accountAddress);


    // using the promise
    // var contractPermissions = yield permissionContractInstance.methods.setAdmin(args.docid, accs).encodeABI();
    this.web3.eth.personal.unlockAccount(adminAccount.accountAddress, adminAccount.accountPwd);
    let rec = await permissionContractInstance.methods.addUser(this.utils.add0x(newPermission.documentID), newPermission.accountAddress).send(
      {
        from: adminAccount.accountAddress,
      });
    console.log(rec.status);

    this.web3.eth.personal.lockAccount(adminAccount.accountAddress);

  }


  async removePermission(permission: Permission, adminAccount: AccountDetails) {

    console.log("permissioningContractAddress" + this.utils.PERMISSION_CONTRACT);


    // check the permisioning contract
    this.permissionABI = await this.utils.getABI();
    const permissionContractInstance = new this.web3.eth.Contract(this.permissionABI, this.utils.PERMISSION_CONTRACT);


    console.log("docid: " + permission.documentID);
    console.log("accs: " + permission.accountAddress);


    // using the promise

    this.web3.eth.personal.unlockAccount(adminAccount.accountAddress, adminAccount.accountPwd);
    let rec = await permissionContractInstance.methods.removeUser(this.utils.add0x(permission.documentID), permission.accountAddress).send(
      {
        from: adminAccount.accountAddress,
      });
    console.log(rec.status);

    this.web3.eth.personal.lockAccount(adminAccount.accountAddress);
  }


  async getRequestedOrganisations(account: AccountDetails): Promise<string[]> {
    console.log("permissioningContractAddress" + this.utils.PERMISSION_CONTRACT);

    let approvalRequestArray = [];


    // check the permisioning contract
    this.permissionABI = await this.utils.getABI();
    const permissionContractInstance = new this.web3.eth.Contract(this.permissionABI, this.utils.PERMISSION_CONTRACT);

    let cnt = 0;
    let add = "";
    approvalRequestArray.length = 0;

    console.log("Account:" + JSON.stringify(account));
    

    for (let i = 0; add != "0x0000000000000000000000000000000000000000"; ++i) {
      console.log("i:"+i);
      
      add = await permissionContractInstance.methods.getRequests(account.accountAddress, i).call(
        {
          from: account.accountAddress,
        });

      console.log("address:"+add);


      if (add == "0x0000000000000000000000000000000000000000") {
        console.log("not found");
        break;
      }
      // need to check
      approvalRequestArray.push(add);
      cnt++;
    }
    console.log(approvalRequestArray);

    return approvalRequestArray;
  }


  async addRequest(userAccountAddress: string, organisation: Organisation) {

    console.log("add request");
    console.log("useraccount:"+userAccountAddress);
    console.log("organisation:"+organisation.accountID);
    
    
    console.log("permissioningContractAddress" + this.utils.PERMISSION_CONTRACT);


    // check the permisioning contract
    this.permissionABI = await this.utils.getABI();
    const permissionContractInstance = new this.web3.eth.Contract(this.permissionABI, this.utils.PERMISSION_CONTRACT);



    // using the promise
    // var contractPermissions = yield permissionContractInstance.methods.setAdmin(args.docid, accs).encodeABI();
    await this.web3.eth.personal.unlockAccount(organisation.accountAddress, organisation.accountPwd);
    permissionContractInstance.methods.addRequest(userAccountAddress).send(
      {
        from: organisation.accountAddress,
      }).then(receipt => {
        console.log(receipt);
        this.web3.eth.personal.lockAccount(organisation.accountAddress)
          .then(console.log('Account locked!'));
      })
    console.log("returning");

    return "Success";

  }


  async getApprovedUsers(account: AccountDetails): Promise<string[]>{
    console.log("permissioningContractAddress" + this.utils.PERMISSION_CONTRACT);

    let approvedArray = [];


    // check the permisioning contract
    this.permissionABI = await this.utils.getABI();
    const permissionContractInstance = new this.web3.eth.Contract(this.permissionABI, this.utils.PERMISSION_CONTRACT);

    approvedArray.length = 0;

    console.log("Account:" + JSON.stringify(account));


    approvedArray = await permissionContractInstance.methods.getUsers(this.utils.add0x(account.documentID)).call(
        {
          from: account.accountAddress,
        });

    console.log("address:" + approvedArray);

    console.log(approvedArray);

    return approvedArray;
  }

}
