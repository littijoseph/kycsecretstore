import { Injectable } from '@angular/core';

import { UtilsHelperService } from './utils-helper.service';
import { SecretStoreService } from './secret-store.service';
import { HttpClient } from '@angular/common/http';

import { AccountDetails } from '../interfaces/account-details';

//import * as Web3 from 'web3';
import Web3 from 'web3';

import { Organisation } from '../interfaces/organisation';


@Injectable({
  providedIn: 'root'
})
export class OrganisationAccountHelperService {

  private web3: Web3;

  constructor(private http: HttpClient, private ss: SecretStoreService, private utils: UtilsHelperService) {
    this.web3 = new Web3(new Web3.providers.HttpProvider(this.utils.HTTP_RPC_COMMON));
    console.log("provuder:" + JSON.stringify(this.web3.currentProvider));
    
    ss.setSecretStoreServiceParms(this.web3, this.utils.HTTP_SS_REG1);

   }


  /**
   * Function to create account
   * @param accountName account holder name
   * @param accountID user id
   * @param accountPwd user password
   */
  async createAccount(organisation: Organisation) {

    console.log("inside create Account service");


    //create a new account 
    let recoveryPhrase = await this.ss.generateSecretPhrase();

    console.log("recoveryPhrase:" + recoveryPhrase);


    let accountAddress = await this.ss.newAccountFromPhrase(recoveryPhrase, organisation.accountPwd);

    organisation.recoveryPhrase = recoveryPhrase.toString();
    organisation.accountAddress = accountAddress.toString();


    console.log("accountId:" + organisation.accountID);
    console.log("accountDetails:" + JSON.stringify(organisation));



    return organisation;


  }

  /**
   * 
   * Function to login to a account 
   * 
   * @param accountID account id given by user in login
   * @param accountPwd account password given by user in login 
   * @returns Details of the saved account
   */

  Login(accountID, accountPwd) {

    let savedAccountDetails = JSON.parse(localStorage.getItem(accountID));
    if (accountPwd === savedAccountDetails.accountPwd) {
      console.log("account logged in succesfully");
      return savedAccountDetails;
    }
    else {
      return null;
    }

  }

}
