import { Injectable } from '@angular/core';
import { UtilsHelperService } from './utils-helper.service';
import { SecretStoreService } from './secret-store.service';
import { HttpClient } from '@angular/common/http';

//import * as Web3 from 'web3';
import Web3 from 'web3';

// Interfaces used imported below
import { EncryptedDocument } from '../interfaces/encrypted-document';
import { DecryptionKey } from '../interfaces/decryption-key';


@Injectable({
  providedIn: 'root'
})
export class DocumentDecryptHelperService {
  
  private web3: Web3;
  private kycDocumenttoDecrypt: EncryptedDocument;

  constructor(private http: HttpClient, private ss: SecretStoreService, private utils: UtilsHelperService) {

    this.web3 = new Web3(new Web3.providers.HttpProvider(this.utils.HTTP_RPC_COMMON));
    ss.setSecretStoreServiceParms(this.web3, this.utils.HTTP_SS_REG1);
    this.kycDocumenttoDecrypt = <EncryptedDocument>{};
   }


  /**
  * Function to decrypt document
  *
  * @param JSON   kyc document data
  * @param string accountaddress user account to use for encryption
  * @param string accountpwd user account password to use for encryption
  * @returns (string) document decypted document
  */
  async decryptDocument(kycDocumenttoDecrypt, decryptAccountAddress, decryptAccountPwd) {
  
      
try{
      console.log("account address used for decrypting: " + decryptAccountAddress);
      console.log("account password used for decrypting: " + decryptAccountPwd);

      // receives the message: document ID and encrypted document
      this.kycDocumenttoDecrypt = kycDocumenttoDecrypt;
      console.log("Message received: " + JSON.stringify(kycDocumenttoDecrypt));

      // 1. signing the document ID by the 
      const signedDoc = await this.ss.signRawHash(decryptAccountAddress, decryptAccountPwd, kycDocumenttoDecrypt.docID);
      console.log("Doc ID signed: " + signedDoc);

      // 2. Let's retrieve the keys
    let decryptionKeys: DecryptionKey;
    decryptionKeys = await this.ss.session.shadowRetrieveDocumentKey(kycDocumenttoDecrypt.docID, signedDoc);
      console.log("Decryption keys retrieved: " + JSON.stringify(decryptionKeys));

      // 3. Decrypt document
      //decryptedSecret, commonPoint, decryptShadows, encryptedDocument
    const hexDocument = await this.ss.shadowDecrypt(decryptAccountAddress, decryptAccountPwd,
        decryptionKeys.decrypted_secret,
        decryptionKeys.common_point,
        decryptionKeys.decrypt_shadows,
        kycDocumenttoDecrypt.encryptedDocument);
      console.log("Decrypted hex document: " + hexDocument);

      // 3.1 hex to str
    const document = this.web3.utils.toAscii(hexDocument.toString());
      console.log("Decrypted document: " + document);
      return document;
    }
    catch(err){
      console.log("error:"+err);
      return null;
    }

  }

}
