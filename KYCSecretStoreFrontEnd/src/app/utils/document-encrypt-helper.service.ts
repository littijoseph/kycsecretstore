import { Injectable } from '@angular/core';
import { UtilsHelperService } from './utils-helper.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { SecretStoreService } from './secret-store.service';
import { AccountDetails } from '../interfaces/account-details';

// Interfaces used imported below
import { EncryptedDocument } from '../interfaces/encrypted-document';
import { HttpClient } from '@angular/common/http';
import { DocumentKey } from '../interfaces/document-key';


//import * as Web3 from 'web3';
import Web3 from 'web3';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentEncryptHelperService {

  private web3: any;
  // private utils: UtilsHelperService;
  // private ss: SecretStoreService;

  private kycdDocumentSaved: EncryptedDocument;

  constructor(private http: HttpClient, private ss: SecretStoreService, private utils: UtilsHelperService) {

    this.web3 = new Web3(new Web3.providers.HttpProvider(this.utils.HTTP_RPC_COMMON));
  ss.setSecretStoreServiceParms(this.web3, this.utils.HTTP_SS_REG1);
  this.kycdDocumentSaved = <EncryptedDocument>{};
}


/**
* Function to encrypt document
*
* @param string document document data
* @param string accountaddress user account to use for encryption
* @param string accountpwd user account password to use for encryption
* @returns JSON kycdDocumentSaved kyc document saved details
*/
async encryptDocument(document, accountDetails:AccountDetails) {


    console.log('document: ' + document);
    console.log('account address: ' + accountDetails.accountAddress);
    console.log('account password: ' + accountDetails.accountPwd);


    // // 1. we generate a hash of the document name as the document ID
    const docID = await this.utils.getSHA256hash(accountDetails.accountID);
    console.log('doc ID: ' + docID);

    this.kycdDocumentSaved.docID = docID;

    // // 2.1 we sign the document key id
    const signedDocID = await this.ss.signRawHash(accountDetails.accountAddress, accountDetails.accountPwd, docID);
    console.log('signed doc ID: ' + signedDocID);

    // // 2.2 we generate the secret store server key
    let serverKey;
    try {
      // threshold is chosen to be 1 like in the official tutorial
      serverKey = await this.ss.session.generateServerKey(docID, signedDocID, 0);
    } catch (err) {
      console.log(err);
      
      if (err.response.body === '"\\"Server key with this ID is already generated\\""') {
        console.log('Erase Secure Store database or use different one.');
        throw err;
      }
      throw err;
    }
    console.log("Server key public part: " + JSON.stringify(serverKey));

    // 3. Generate document key
  let documentKey: DocumentKey;
  documentKey = await this.ss.generateDocumentKey(accountDetails.accountAddress, accountDetails.accountPwd, serverKey);
    console.log("Document key" + JSON.stringify(documentKey));

  

    // 4.-1 the document in hex format
    const hexDocument = this.web3.utils.toHex(document);
    console.log("Hex document: " + hexDocument);

    // 4. Document encryption
    const encryptedDocument = await this.ss.encrypt(accountDetails.accountAddress, accountDetails.accountPwd, documentKey.encrypted_key, hexDocument);
    console.log("Encrypted secret document: " + encryptedDocument);

    this.kycdDocumentSaved.encryptedDocument = encryptedDocument;

    // 5. Store the generated document key
    const res = await this.ss.session.storeDocumentKey(docID, signedDocID, documentKey.common_point, documentKey.encrypted_point);

    // fs.writeFileSync("./sent_message.json", JSON.stringify(messageToSend));
    console.log(JSON.stringify(this.kycdDocumentSaved));
    return this.kycdDocumentSaved;
}
}
