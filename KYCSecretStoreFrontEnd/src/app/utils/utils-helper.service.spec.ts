import { TestBed } from '@angular/core/testing';

import { UtilsHelperService } from './utils-helper.service';

describe('UtilsHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilsHelperService = TestBed.get(UtilsHelperService);
    expect(service).toBeTruthy();
  });
});
