import { TestBed } from '@angular/core/testing';

import { CustomerAccountHelperService } from './customer-account-helper.service';

describe('CustomerAccountHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerAccountHelperService = TestBed.get(CustomerAccountHelperService);
    expect(service).toBeTruthy();
  });
});
