import { Injectable } from '@angular/core';
import { UtilsHelperService } from './utils-helper.service';
import { SecretStoreService } from './secret-store.service';
import { HttpClient } from '@angular/common/http';


//import * as Web3 from 'web3';
import Web3 from 'web3';
import { AccountDetails } from '../interfaces/account-details';

@Injectable({
  providedIn: 'root'
})
export class FundingHelperService {
  private web3: Web3;
  constructor(private http: HttpClient, private utils: UtilsHelperService) { 
    this.web3 = new Web3(new Web3.providers.HttpProvider(this.utils.HTTP_RPC_COMMON));
    console.log("Web3 version:" + this.web3.version);

  }


  /**
   * 
   * Creates a secret phrase that can be associated with an account.
   * 
   * @returns {Promise<String>} The secret phrase.
*/

  async fundAccount(accountAddress, verbose = true) {
    console.log("Fund Newly Created Account");
    let amount = "0x8AC7230489E80000";
    return new Promise((resolve, reject) => {
      this.web3.currentProvider.send({
        jsonrpc: '2.0',
        method: 'personal_sendTransaction',
        params: [{
          "from": "0x00a329c0648769A73afAc7F9381E08FB43dBEA72",
          "to": accountAddress,
          "value": amount,
        }, ""],
        id: 0
      }, (e, r) => {
        console.log(e, r);
        if (e) {
          console.log(e);
          if (verbose) this.utils.logError(e);
          reject(e);
        } else if (r.error !== undefined) {
          console.log(r);
          if (verbose) this.utils.logError(r.error);
          reject(r.error);
        } else {
          console.log(r);
          resolve(r.result);
        }
      });

    });
  }
}
