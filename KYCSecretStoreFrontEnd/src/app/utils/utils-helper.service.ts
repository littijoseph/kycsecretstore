import { Injectable } from '@angular/core';
// import * as TruffleContract from 'truffle-contract';
 import * as crypto from 'crypto-js';

 import { createHash } from 'crypto-browserify';
import * as $ from 'jquery';// import Jquery here


declare let require: any;

import tokenAbi from '../../../../build/contracts/PermissioningRegistry.json';

import * as contractJson from '../../assets/contracts/PermissioningRegistry.json';

@Injectable({
  providedIn: 'root'
})
export class UtilsHelperService {

  // // Config variables
  // readonly holderType = {
  //   'milkBank': '01', 'baby': '02'
  // };

  

  readonly PERMISSION_CONTRACT = '0x40f3E7A1C80cb1C608B4c50D8ADb4b404De878c1';
  readonly HTTP_RPC_COMMON = 'http://localhost:8550';
  readonly HTTP_SS_REG1 = "http://127.0.0.1:8083";
  readonly HTTP_SS_REG2 = "http://127.0.0.1:8091";
  readonly HTTP_SS_REG3 = "http://127.0.0.1:8092";

  // readonly tokenAbi = require(‘../../../build/contracts/PermissioningRegistry.json’);

  constructor() { }


  public __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
    function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
    function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

public remove0x(str) {
  if (str.startsWith("0x")) {
    return str.slice(2);
  }
  return str;
}

public add0x(str) {
  if (!str.startsWith("0x")) {
    return "0x" + str;
  }
  return str;
}

public connectionsHTTPRPC() {
  return {
    httpRpcCommon: this.HTTP_RPC_COMMON
  };
}

public connectionsHTTPSS() {
  return {
    httpSSRegulator1: this.HTTP_SS_REG1,
    httpSSRegulator2: this.HTTP_SS_REG2,
    httpSSRegulator3: this.HTTP_SS_REG3
  };
}


public getPermissionContractAddress() {
  return {
    permissioningContractAddress: this.PERMISSION_CONTRACT
  };
}

//  private hash = (x) => createHash('sha256').update(x, 'utf-8').digest('hex');

public  getSHA256hash(message) {
  return new Promise<string>((resolve, reject) => {
    try {
      // const hash = this.hash(message);
      const hash = crypto.SHA256(message);
      resolve(hash.toString());
    } catch (err) {
      reject(err);
    }
  });
}


/**
 * Removes enclosing double quotes from a string
 * 
 * @param {String} str The string
 * @returns {String} The string without enclosing double quotes
 */
public removeEnclosingDQuotes(str) {
  return str.replace(/^"(.*)"$/, '$1');
}

/**
 * Logs relevant information from a failed http response.
 * 
 * @param {Object} response The response object
 * @param {Object} body The body of the response
 * @param {Object} options Options object of the request
 */
  public logFailedResponse(response, body, options) {
  console.log("Request failed");
  console.log("StatusCode: " + response.statusCode);
  console.log("StatusMessage: " + response.statusMessage);
  console.log("Body: " + body);
  console.log("Request options: " + JSON.stringify(options));
}

/**
 * Logs the error object.
 * 
 * @param {Error} e Error object
 */
  public logError(e) {
  console.log("Error:");
  console.log(e);
}

async getABI(){
  // let data: string;
  // $.getJSON('../../../../build/contracts/PermissioningRegistry.json', function (data: string) {
  //   console.log(JSON.stringify(data));
  //   this.data = data;
  // });
  // return data;

  // let apiUrl = '../assets/data/api/advantage.json';
  // return this.http.get(apiUrl)
  //   .map((response: Response) => {
  //     const data = response.json();
  //     return data;
  //   });

  const contractABI = contractJson.abi;
  console.log(contractABI);
  return contractABI;
}


}
