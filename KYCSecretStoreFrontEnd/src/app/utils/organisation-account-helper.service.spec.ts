import { TestBed } from '@angular/core/testing';

import { OrganisationAccountHelperService } from './organisation-account-helper.service';

describe('OrganisationAccountHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrganisationAccountHelperService = TestBed.get(OrganisationAccountHelperService);
    expect(service).toBeTruthy();
  });
});
