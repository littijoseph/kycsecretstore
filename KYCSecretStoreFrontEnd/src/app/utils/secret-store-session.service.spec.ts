import { TestBed } from '@angular/core/testing';

import { SecretStoreSessionService } from './secret-store-session.service';

describe('SecretStoreSessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecretStoreSessionService = TestBed.get(SecretStoreSessionService);
    expect(service).toBeTruthy();
  });
});
