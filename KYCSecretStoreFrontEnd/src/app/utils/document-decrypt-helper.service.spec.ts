import { TestBed } from '@angular/core/testing';

import { DocumentDecryptHelperService } from './document-decrypt-helper.service';

describe('DocumentDecryptHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentDecryptHelperService = TestBed.get(DocumentDecryptHelperService);
    expect(service).toBeTruthy();
  });
});
