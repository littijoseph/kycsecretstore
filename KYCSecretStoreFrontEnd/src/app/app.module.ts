import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomerdashboardComponent } from './customerdashboard/customerdashboard.component';
import { CustomerhomeComponent } from './customerhome/customerhome.component';
import { CustomerregisterComponent } from './customerregister/customerregister.component';
import { CustomerloginComponent } from './customerlogin/customerlogin.component';
import { AddkycdetailsComponent } from './addkycdetails/addkycdetails.component';
import { ViewkycrequestComponent } from './viewkycrequest/viewkycrequest.component';
import { KycapprovedlistComponent } from './kycapprovedlist/kycapprovedlist.component';
import { OrganisationdashboardComponent } from './organisationdashboard/organisationdashboard.component';
import { OrganisationregisterComponent } from './organisationregister/organisationregister.component';
import { OrganisationloginComponent } from './organisationlogin/organisationlogin.component';
import { OrganisationhomeComponent } from './organisationhome/organisationhome.component';
import { HttpClientModule } from '@angular/common/http';
import { OrganisationdetailsComponent } from './organisationdetails/organisationdetails.component';
import { ViewkycdetailsComponent } from './viewkycdetails/viewkycdetails.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CustomerdashboardComponent,
    CustomerhomeComponent,
    CustomerregisterComponent,
    CustomerloginComponent,
    AddkycdetailsComponent,
    ViewkycrequestComponent,
    KycapprovedlistComponent,
    OrganisationdashboardComponent,
    OrganisationregisterComponent,
    OrganisationloginComponent,
    OrganisationhomeComponent,
    OrganisationdetailsComponent,
    ViewkycdetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
