import { Injectable } from '@angular/core';
import { DocumentDecryptHelperService } from './utils/document-decrypt-helper.service';
import { DocumentEncryptHelperService } from './utils/document-encrypt-helper.service';
import { UtilsHelperService } from './utils/utils-helper.service';
import { CustomerAccountHelperService } from './utils/customer-account-helper.service';
import { OrganisationAccountHelperService } from './utils/organisation-account-helper.service';


import * as Web3 from 'web3';
import * as TruffleContract from 'truffle-contract';
import { HttpClient } from '@angular/common/http';

// Interfaces used imported below
import { EncryptedDocument } from './interfaces/encrypted-document';
import { AccountDetails } from './interfaces/account-details';
import { KycData } from './interfaces/kyc-data';
import { Organisation } from './interfaces/organisation';
import { PermissionContractHelperService } from './utils/permission-contract-helper.service';
import { FundingHelperService } from './utils/funding-helper.service';
import { Permission } from './interfaces/permission';


@Injectable({
  providedIn: 'root'
})
export class EthereumServicesService {


  // public DM_PASSPHARSE = 'This is my dummy passpharse for batcher key pair';

  private web3: any;
  private currentAccountDetails: AccountDetails;
  private currentOrganisationAccountDetails: Organisation;


  // private batchSigner: any;
  // public batchPublicKey: any;


  constructor(private http: HttpClient,
    private documentEncryptHelper: DocumentEncryptHelperService,
    private documentDecryptHelper: DocumentDecryptHelperService,
    private customerAccountHelper: CustomerAccountHelperService,
    private organisationAccountHelper: OrganisationAccountHelperService,
    private permissionContractHelper: PermissionContractHelperService,
    private fundingHelper: FundingHelperService,
    private utils: UtilsHelperService) {

    this.currentAccountDetails = <AccountDetails>{};
    this.currentOrganisationAccountDetails = <Organisation>{};

  }


  getAccountDetails(accountID = null) {
    if (this.currentAccountDetails != null) {
      return this.currentAccountDetails;
    }
    else if (accountID != null) {
      if (JSON.parse(localStorage.getItem(accountID)) != null) {
        this.currentAccountDetails = JSON.parse(localStorage.getItem(accountID));
        return this.currentAccountDetails;
      }
    }
    return null;
  }

  getOrganisationAccountDetails(accountID = null) {
    console.log("getOrganisationCalled");

    if (this.currentOrganisationAccountDetails != null) {
      return this.currentOrganisationAccountDetails;
    }
    if (accountID != null) {
      if (JSON.parse(localStorage.getItem(accountID)) != null) {
        this.currentOrganisationAccountDetails = JSON.parse(localStorage.getItem(accountID));
        return this.currentOrganisationAccountDetails;
      }
    }
    return null;
  }


  async getKycDocumentSaved(accountID) {
    const docID = await this.utils.getSHA256hash(accountID);
    let kycDocument: EncryptedDocument;
    kycDocument = JSON.parse(localStorage.getItem(docID));
    return kycDocument;
  }

  /**
   * Service to encrypt document
   *
   * @param {string} document document data
   * @param {string} accountaddress user account to use for encryption
   * @param {string} accountpwd user account password to use for encryption
   */
  async encryptDocument(document, currentAccount: AccountDetails) {

    let documentSaved = await this.documentEncryptHelper.encryptDocument(document, currentAccount);
    console.log("document saved:" + JSON.stringify(documentSaved));

    currentAccount.documentID = documentSaved.docID;
    localStorage.setItem(currentAccount.accountID, JSON.stringify(currentAccount));
    localStorage.setItem(currentAccount.documentID, JSON.stringify(documentSaved));

    let permissionUpdate = await this.permissionContractHelper.setAdminPermission(currentAccount);

    return (documentSaved);

  }

  /**
   * Service to decrypt document
   *
   * @param {JSON} kycDocumenttoDecrypt document data
   * @param {string} accountaddress user account to use for encryption
   * @param {string} accountpwd user account password to use for encryption
   * @returns {string} decryptedDocument 
   */
  async decryptDocument(kycDocumenttoDecrypt, accountaddress, accountpwd) {

    let decryptedDocument = await this.documentDecryptHelper.decryptDocument(kycDocumenttoDecrypt, accountaddress, accountpwd);
    console.log("Decrypted Document:" + decryptedDocument);
    return (decryptedDocument);
  }

  /**
   * service to create account for customer
   * 
   * @param accountName Account holder name
   * @param accountID Account user id
   * @param accountPwd Account pwd
   */
  async createAccountCustomer(accountName, accountID, accountPwd) {
    let accountDetails = await this.customerAccountHelper.createAccount(accountName, accountID, accountPwd);
    // saving account details
    localStorage.setItem(accountID, JSON.stringify(accountDetails));
    localStorage.setItem(accountDetails.accountAddress, accountID);
    let result = this.fundingHelper.fundAccount(accountDetails.accountAddress);
    return accountDetails;
  }


  /**
   * service to customer login 
   * 
   * @param accountID 
   * @param accountPwd 
   * @returns customer details 
   */
  customerLogin(accountID, accountPwd) {
    let accountDetails = this.customerAccountHelper.Login(accountID, accountPwd);
    if (accountDetails != null) {
      this.currentAccountDetails = accountDetails;
      console.log(this.currentAccountDetails);

    }
    return accountDetails;
  }


  /**
   * service to create account for organisation
   * 
   * @param accountName Account holder name
   * @param accountID Account user id
   * @param accountPwd Account pwd
   */
  async createAccountOrganisation(organisation: Organisation) {
    let accountDetails = await this.organisationAccountHelper.createAccount(organisation);
    // saving account details
    localStorage.setItem(organisation.accountID, JSON.stringify(organisation));
    localStorage.setItem(organisation.accountAddress, organisation.accountID);
    this.fundingHelper.fundAccount(organisation.accountAddress);
    return accountDetails;
  }




  /**
   * service to organisation login 
   * 
   * @param accountID 
   * @param accountPwd 
   * @returns customer details 
   */
  OrganisationLogin(accountID, accountPwd) {
    let accountDetails = this.organisationAccountHelper.Login(accountID, accountPwd);
    if (accountDetails != null) {
      this.currentOrganisationAccountDetails = accountDetails;
      console.log("Login:" + JSON.stringify(this.currentOrganisationAccountDetails));

    }
    return accountDetails;
  }

  async viewCustomerDetails(customerID, organisation: Organisation) {

    let documentSaved: EncryptedDocument;

    documentSaved = await this.getKycDocumentSaved(customerID);
    let decryptedDocument: KycData;
    let decryptedDocumentString = await this.decryptDocument(documentSaved, organisation.accountAddress, organisation.accountPwd)
    decryptedDocument = JSON.parse(decryptedDocumentString);
    console.log("decryptedDocument:" + decryptedDocument);
    return decryptedDocument;
  }



  async getRequestedOrganisationsList(account: AccountDetails): Promise<string[]> {

    let approvalRequestOrganisationIDArray: string[] = [];


    let approvalRequestArray = await this.permissionContractHelper.getRequestedOrganisations(account);
    console.log("apprivalarray:" + approvalRequestArray);

    if (approvalRequestArray != null) {
      approvalRequestArray.forEach(organisationAddress => {
        console.log(organisationAddress);
        console.log(localStorage.getItem(organisationAddress.toLowerCase()));
        approvalRequestOrganisationIDArray.push(localStorage.getItem(organisationAddress.toLowerCase()));
      });
    }
    console.log(approvalRequestOrganisationIDArray);

    return approvalRequestOrganisationIDArray;

  }


  async getApprovedOrganisationsList(account: AccountDetails): Promise<string[]> {
    let approvedOrganisationIDArray: string[] = [];

    let approvedAddressList = await this.permissionContractHelper.getApprovedUsers(account);

    console.log("approvedarray:" + approvedAddressList);

    if (approvedAddressList != null) {
      approvedAddressList.forEach(organisationAddress => {
        organisationAddress = organisationAddress.toLowerCase();
        if (organisationAddress !== account.accountAddress){
        console.log(organisationAddress);
        console.log(localStorage.getItem(organisationAddress));
        approvedOrganisationIDArray.push(localStorage.getItem(organisationAddress.toLowerCase()));
        }
      });
    }
    return approvedOrganisationIDArray;
  }

  async addRequest(customerID, organisation: Organisation): Promise<any> {

    let userAccount: AccountDetails = JSON.parse(localStorage.getItem(customerID));
    await this.permissionContractHelper.addRequest(userAccount.accountAddress, organisation);

  }

  async acceptRequest(organisationID, account: AccountDetails): Promise<any> {

    try {


      let organisation: Organisation = JSON.parse(localStorage.getItem(organisationID));
      let newPermission = <Permission>{};
      newPermission.documentID = account.documentID;
      newPermission.accountAddress = organisation.accountAddress;
      await this.permissionContractHelper.addPermission(newPermission, account);
      return "success";
    }
    catch (err) {
      console.log(err);

    }
  }

  async removePermission(organisationID, account: AccountDetails): Promise<any> {
      
      try {


        let organisation: Organisation = JSON.parse(localStorage.getItem(organisationID));
        let permission = <Permission>{};
        permission.documentID = account.documentID;
        permission.accountAddress = organisation.accountAddress;
        await this.permissionContractHelper.removePermission(permission, account);
        return "success";
      }
      catch (err) {
        console.log(err);

      }
    }

  }

