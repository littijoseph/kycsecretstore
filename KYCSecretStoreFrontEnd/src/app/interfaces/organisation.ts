export interface Organisation {
    accountID: string,
    accountName: string,
    accountReg: string,
    recoveryPhrase: string,
    accountAddress: string,
    accountPwd: string,
    kycNo: number,
    rating: number,

}

