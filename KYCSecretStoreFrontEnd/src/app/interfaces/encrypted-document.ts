export interface EncryptedDocument {
    docID: string;
    encryptedDocument: string;
}

