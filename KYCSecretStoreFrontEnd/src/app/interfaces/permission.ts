export interface Permission {
    documentID: string,
    accountAddress: string,
}
