export interface AccountDetails {
    accountID: string,
    accountName: string,
    recoveryPhrase: string,
    accountAddress: string,
    accountPwd: string,
    documentID: string
}
