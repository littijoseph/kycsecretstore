export interface DecryptionKey {
    decrypted_secret: string,
    common_point: string,
    decrypt_shadows: string
}
