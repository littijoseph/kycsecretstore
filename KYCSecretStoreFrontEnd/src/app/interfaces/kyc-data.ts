export interface KycData {
    customerFullName: string,
    gender: string,
    dob: string,
    address: string,
    nationality: string,
    pan: string,
    email: string,
    phone: string,
    annualIncome: string,

}
