export interface DocumentKey {
    encrypted_key: string,
    common_point: string,
    encrypted_point: string
}
