import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent} from './dashboard/dashboard.component';
import { CustomerdashboardComponent } from './customerdashboard/customerdashboard.component';
import { CustomerregisterComponent} from './customerregister/customerregister.component';
import { CustomerloginComponent} from './customerlogin/customerlogin.component';
import { CustomerhomeComponent } from './customerhome/customerhome.component';
import { AddkycdetailsComponent } from './addkycdetails/addkycdetails.component';
import { ViewkycrequestComponent } from './viewkycrequest/viewkycrequest.component';
import { KycapprovedlistComponent } from './kycapprovedlist/kycapprovedlist.component';
import { OrganisationdashboardComponent } from './organisationdashboard/organisationdashboard.component';
import { OrganisationregisterComponent } from './organisationregister/organisationregister.component';
import { OrganisationloginComponent } from './organisationlogin/organisationlogin.component';
import { OrganisationhomeComponent } from './organisationhome/organisationhome.component';
import { OrganisationdetailsComponent} from './organisationdetails/organisationdetails.component';
import { ViewkycdetailsComponent } from './viewkycdetails/viewkycdetails.component';



import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'customer',
    component: CustomerdashboardComponent,
    children: [
      {
        path: 'customerregister',
        component: CustomerregisterComponent
      },
      {
        path: 'customerlogin',
        component: CustomerloginComponent
      },
    ]
  },
  {
    path: 'customerhome',
    component: CustomerhomeComponent,
    children: [
      {
        path: 'addkycdata',
        component: AddkycdetailsComponent
      },
      {
        path: 'viewkycrequest',
        component: ViewkycrequestComponent
      },
      {
        path: 'kycapprovedlist',
        component: KycapprovedlistComponent
      }

    ]
  },
  {
    path: 'organisation',
    component: OrganisationdashboardComponent,
    children: [
      {
        path: 'organisationregister',
        component: OrganisationregisterComponent
      },
      {
        path: 'organisationlogin',
        component: OrganisationloginComponent
      },
    ]
  },
  {
    path: 'organisationhome',
    component: OrganisationhomeComponent,
    children: [
      {
        path: 'viewOrganisation',
        component: OrganisationdetailsComponent
      },
      {
        path: 'viewkyc',
        component: ViewkycdetailsComponent
      },
      {
        path: 'modifykyc',
        component: KycapprovedlistComponent
      }


    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
