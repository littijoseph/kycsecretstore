import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationdashboardComponent } from './organisationdashboard.component';

describe('OrganisationdashboardComponent', () => {
  let component: OrganisationdashboardComponent;
  let fixture: ComponentFixture<OrganisationdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
