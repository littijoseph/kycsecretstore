import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';
import { AccountDetails } from '../interfaces/account-details';
@Component({
  selector: 'app-customerhome',
  templateUrl: `./customerhome.component.html`,
  styles: []
})
export class CustomerhomeComponent implements OnInit {

  public currentAccount: AccountDetails;

  constructor(private router: Router, private Service: EthereumServicesService) { 
    this.currentAccount = <AccountDetails>{};
  }

  ngOnInit() {
    this.currentAccount = this.Service.getAccountDetails();
    console.log(this.currentAccount);
  }

}
