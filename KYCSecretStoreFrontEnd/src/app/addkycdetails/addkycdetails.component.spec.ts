import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddkycdetailsComponent } from './addkycdetails.component';

describe('AddkycdetailsComponent', () => {
  let component: AddkycdetailsComponent;
  let fixture: ComponentFixture<AddkycdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddkycdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddkycdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
