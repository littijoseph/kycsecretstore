import { Component, OnInit } from '@angular/core';
import { EthereumServicesService } from '../ethereum-services.service';
import { AccountDetails } from '../interfaces/account-details';
import { KycData } from '../interfaces/kyc-data';
import { Router } from '@angular/router';
import { EncryptedDocument } from '../interfaces/encrypted-document';

@Component({
  selector: 'app-addkycdetails',
  templateUrl: `./addkycdetails.component.html`,
  styles: []
})
export class AddkycdetailsComponent implements OnInit {
  public errorMsg: any;
  // public customerFullName: string = '';
  // public gender: string = '';
  // public dob: string = '';
  // public address: string = '';
  // public nationality: string = '';
  // public pan: string = '';
  // public email: string = '';
  // public phone: string = '';
  // public annualIncome: string = '';

  public kycData: KycData;

  public currentAccount: AccountDetails;


  constructor(private router: Router, private Service: EthereumServicesService) {
    this.currentAccount = <AccountDetails>{};
    this.kycData = <KycData>{};
  }

  async ngOnInit() {
    this.currentAccount = this.Service.getAccountDetails();
    console.log(this.currentAccount);
    this.kycData.customerFullName = this.currentAccount.accountName;
    let documentSaved: EncryptedDocument;
    documentSaved = await this.Service.getKycDocumentSaved(this.currentAccount.accountID);
    const decryptedDocument = await this.Service.decryptDocument(documentSaved, this.currentAccount.accountAddress, this.currentAccount.accountPwd)
    console.log("decryptedDocument:" + decryptedDocument);
    if (decryptedDocument != null) {
      this.kycData = JSON.parse(decryptedDocument);
    }



  }

  async addKyc(event) {
    const result = await this.Service.encryptDocument(
      JSON.stringify(this.kycData),
      this.currentAccount
    )
      .catch(error => {
        console.log(error);
      });
    if (result != null) {

      alert("KYC has been successfully added to the Blockchain!");
    }
  }
}
