import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';
import { Organisation} from '../interfaces/organisation';

@Component({
  selector: 'app-organisationregister',
  templateUrl: './organisationregister.component.html',
  styles: []
})
export class OrganisationregisterComponent implements OnInit {
  public errorMsg: any;
  // public organisationFullName: string = '';
  // public organisationReg: string = '';
  // public organisationID: string = '';
  // public organisationPassword: string = '';

  public organisation: Organisation;
  constructor(private router: Router, private Service: EthereumServicesService) {
    this.organisation = <Organisation>{};
   }

  ngOnInit() {
  }

 async organisationregister(event) {

   const result = await this.Service.createAccountOrganisation(this.organisation)
      .catch(error => {
        console.log(error);
      });
    if (result != null) {
      alert(this.organisation.accountName + " has been successfully added to the network!");
      this.router.navigate(['/organisation/organisationlogin']);
    }

  }

}
