import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewkycrequestComponent } from './viewkycrequest.component';

describe('ViewkycrequestComponent', () => {
  let component: ViewkycrequestComponent;
  let fixture: ComponentFixture<ViewkycrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewkycrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewkycrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
