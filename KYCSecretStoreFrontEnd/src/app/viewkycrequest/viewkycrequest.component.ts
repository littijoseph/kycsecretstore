import { Component, OnInit } from '@angular/core';
import { Organisation } from '../interfaces/organisation';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';
import { AccountDetails } from '../interfaces/account-details';

@Component({
  selector: 'app-viewkycrequest',
  templateUrl: `./viewkycrequest.component.html`,
  styles: []
})
export class ViewkycrequestComponent implements OnInit {

  public requestedOrganisationList: string[];
  public currentAccount: AccountDetails;


  constructor(private router: Router, private Service: EthereumServicesService) {
    this.currentAccount = <AccountDetails>{};
    this.requestedOrganisationList = [];
  }

  async ngOnInit() {
    this.currentAccount = this.Service.getAccountDetails();
    console.log(this.currentAccount);
    this.requestedOrganisationList = await this.Service.getRequestedOrganisationsList(this.currentAccount);
    console.log(this.requestedOrganisationList);

  }


  async acceptRequest(event, index) {

    console.log("accept called, index:" + index);
    let organisationSelected = this.requestedOrganisationList[index];
    console.log("Organisaton Selected:" + organisationSelected);

    let response = await this.Service.acceptRequest(organisationSelected, this.currentAccount);
    console.log('response' + response);

    // this.Data.acceptMilk(transfer);
    // this.refreshDonorTransferList();

  }
  async rejectRequest(event, index) {
    // console.log("rejectmilk called, index:" + index);
    // let transfer = this.holderTransferGroup[index];
    // console.log('selected transfer details:' + JSON.stringify(transfer));


    // this.Data.rejectMilk(transfer);
    // this.refreshDonorTransferList();

  }

}
