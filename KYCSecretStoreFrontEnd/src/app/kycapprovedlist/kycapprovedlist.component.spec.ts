import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycapprovedlistComponent } from './kycapprovedlist.component';

describe('KycapprovedlistComponent', () => {
  let component: KycapprovedlistComponent;
  let fixture: ComponentFixture<KycapprovedlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycapprovedlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycapprovedlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
