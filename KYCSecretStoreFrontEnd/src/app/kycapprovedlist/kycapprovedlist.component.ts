import { Component, OnInit } from '@angular/core';
import { Organisation } from '../interfaces/organisation';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';
import { AccountDetails } from '../interfaces/account-details';

@Component({
  selector: 'app-kycapprovedlist',
  templateUrl: `./kycapprovedlist.component.html`,
  styles: []
})
export class KycapprovedlistComponent implements OnInit {

  public approvedOrganisationList: string[];
  public currentAccount: AccountDetails;

  constructor(private router: Router, private Service: EthereumServicesService) {
    this.currentAccount = <AccountDetails>{};
    this.approvedOrganisationList = [];
   }

  async ngOnInit() {
    this.currentAccount = this.Service.getAccountDetails();
    console.log(this.currentAccount);
    this.approvedOrganisationList = await this.Service.getApprovedOrganisationsList(this.currentAccount);
    console.log(this.approvedOrganisationList);

  }

  
  async removePermission(event, index) {

    console.log("accept called, index:" + index);
    let organisationSelected = this.approvedOrganisationList[index];
    console.log("Organisaton Selected:" + organisationSelected);

    let response = await this.Service.removePermission(organisationSelected, this.currentAccount);
    console.log('response' + response);
  }

}
