import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';

import { Organisation } from '../interfaces/organisation';

@Component({
  selector: 'app-organisationlogin',
  templateUrl: './organisationlogin.component.html',
  styles: []
})
export class OrganisationloginComponent implements OnInit {
  public errorMsg: any;
  
  public organisation: Organisation;

  constructor(private router: Router, private Service: EthereumServicesService) { 
    this.organisation = <Organisation>{};
  }

  ngOnInit() {
  }
  organisationlogin(event) {
    let savedCustomerDetails = this.Service.OrganisationLogin(this.organisation.accountID, this.organisation.accountPwd);
    if (savedCustomerDetails != null) {
      alert(this.organisation.accountID + ' Logged in successfully');
      this.router.navigate(['/organisationhome/viewOrganisation']);
    }
    else {
      alert("Incorrect credentials");
      this.organisation.accountID = '';
      this.organisation.accountPwd = '';
    }
  }

}
