import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationloginComponent } from './organisationlogin.component';

describe('OrganisationloginComponent', () => {
  let component: OrganisationloginComponent;
  let fixture: ComponentFixture<OrganisationloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
