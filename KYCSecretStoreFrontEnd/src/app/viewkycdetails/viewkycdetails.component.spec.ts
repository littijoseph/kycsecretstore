import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewkycdetailsComponent } from './viewkycdetails.component';

describe('ViewkycdetailsComponent', () => {
  let component: ViewkycdetailsComponent;
  let fixture: ComponentFixture<ViewkycdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewkycdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewkycdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
