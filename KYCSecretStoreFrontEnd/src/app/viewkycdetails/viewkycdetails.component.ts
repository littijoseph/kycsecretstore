import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EthereumServicesService } from '../ethereum-services.service';
import { KycData } from '../interfaces/kyc-data';
import { Organisation } from '../interfaces/organisation';

@Component({
  selector: 'app-viewkycdetails',
  templateUrl: `./viewkycdetails.component.html`,
  styles: []
})
export class ViewkycdetailsComponent implements OnInit {
  public showCustomerDetails: boolean;
  public searchCustomerID: string;
  public kycData: KycData;

  public currentOrganisation: Organisation;


  constructor(private router: Router, private Service: EthereumServicesService) {
    this.currentOrganisation = <Organisation>{};
    this.kycData = <KycData>{};
   }

  async ngOnInit() {
    this.showCustomerDetails = false;
    this.currentOrganisation = this.Service.getOrganisationAccountDetails();
    console.log(this.currentOrganisation);
  }

  async customersearch(event) {
    let decryptedDocument = await this.Service.viewCustomerDetails(this.searchCustomerID, this.currentOrganisation);
    if (decryptedDocument != null )
    {
      this.kycData = decryptedDocument;
      this.showCustomerDetails= true;
    }
    else 
    { 
      this.showCustomerDetails = false;
      var l = confirm('Access denied! Take permission from the customer to proceed. \n Click OK to request permission.');
      if (l == true) {
       let rc = await this.Service.addRequest(this.searchCustomerID, this.currentOrganisation);
      }
    }
  }

}
